var x=6, y=14, z=4;
x += y - x++ * z; //1. x++ * 2 = 24.
//2. y - 24 = -10.
//3. x += -10 = -4.
document.write("<p>"+ "x += y - x++ * z = " + x +"</p>"+ "<br>");
var x=6, y=14, z=4;
z = --x - y * 5;//1. --x = 5.
//2. y * 5 = 70.
//3. 5 - 70 = -65.
document.write("<p>"+ "z = --x - y * 5 = " + z +"</p>"+ "<br>");
var x=6, y=14, z=4;
y /= x + 5 % z;//1. 5 % z = 1
//2. x + 1 = 7.
//3. y /= 7 = 2.
document.write("<p>"+ "y /= x + 5 % z = " + y +"</p>"+ "<br>");
var x=6, y=14, z=4;
z -= x++ + y * 5;//1. x++.
//2. y * 5 = 70.
//3. x + 70 = 76.
//4. z -= 76 = -72.
document.write("<p>"+ "z -= x++ + y * 5 = " + z +"</p>"+ "<br>");
var x=6, y=14, z=4;
x = y - x++ * z;//1. x++ * z = 24.
//2. y - 24 = -10.
document.write("<p>"+ "x = y - x++ * z = " + x +"</p>"+ "<br>");